//! # Ranges
//! This crate provides a generic alternative to core/std ranges, set-operations to work with them
//! and a range set that can efficiently store them with the least amount of memory
//! possible.
//!
//! # Features
//! - `From` implementations for all core/std ranges
//! - open ranges like `(3, 10]`
//! - support for [`RangeBounds<T>`]
//! - iterators (even for unbound ranges when the domain has a minimum)
//! - [`Display`] implementations for single and set ranges (with format argument pass-through)
//! - Operators like `|` and `^` for their respective operation
//! - [`Domain`] implementations for types like [`bool`] and [`char`]
//!
//! [`RangeBounds<T>`]: https://doc.rust-lang.org/stable/core/ops/trait.RangeBounds.html
//! [`Display`]: https://doc.rust-lang.org/stable/core/fmt/trait.Display.html
//! [`Domain`]: trait.Domain.html
//! [`bool`]: trait.Domain.html#impl-Domain-for-bool
//! [`char`]: trait.Domain.html#impl-Domain-for-char

#![cfg_attr(not(any(test, feature = "arbitrary")), no_std)]
extern crate alloc;

pub use crate::domain::Domain;
pub use crate::generic_range::{
    relation::{Arrangement, Relation},
    GenericRange, OperationResult,
};
pub use crate::ranges::Ranges;

/// Trait and required function to correctly distinguish between discrete and continuous types.
/// Already contains base implementations for all primitive and some `Ord` implementing types.
mod domain;
/// Everything related to a single generic range.
mod generic_range;
/// Range-set and combinatory logic.
mod ranges;

/// Re-exports of optional feature crates, to ease usage with this crate.
#[cfg(any(
    feature = "noisy_float",
    feature = "num-bigint",
    feature = "arbitrary",
    feature = "proptest"
))]
pub mod exports {
    #[cfg(feature = "arbitrary")]
    pub use arbitrary;
    #[cfg(feature = "noisy_float")]
    pub use noisy_float;
    #[cfg(feature = "num-bigint")]
    pub use num_bigint;
    #[cfg(feature = "proptest")]
    pub use proptest;
}
