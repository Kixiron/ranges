use core::ops::Bound;

use crate::GenericRange;

impl<T: PartialOrd> GenericRange<T> {
    /// Returns true if the given `item` is contained in the range.
    ///
    /// # Example
    ///```
    /// use ranges::GenericRange;
    ///
    /// let range = GenericRange::from(1..=5);
    /// assert!(range.contains(&3));
    ///```
    #[must_use]
    pub fn contains(&self, item: &T) -> bool {
        Self::generic_start_end_contains(&self.start, &self.end, item)
    }

    /// Returns true if the given `item` is contained between the start and end.
    pub(crate) fn generic_start_end_contains(start: &Bound<T>, end: &Bound<T>, item: &T) -> bool {
        match (start, end) {
            (&Bound::Unbounded, &Bound::Unbounded) => true,

            (&Bound::Unbounded, &Bound::Excluded(ref y)) => item < y,
            (&Bound::Unbounded, &Bound::Included(ref y)) => item <= y,

            (&Bound::Excluded(ref x), &Bound::Unbounded) => item > x,
            (&Bound::Included(ref x), &Bound::Unbounded) => item >= x,

            (&Bound::Excluded(ref x), &Bound::Excluded(ref y)) => item > x && item < y,
            (&Bound::Excluded(ref x), &Bound::Included(ref y)) => item > x && item <= y,
            (&Bound::Included(ref x), &Bound::Excluded(ref y)) => item >= x && item < y,
            (&Bound::Included(ref x), &Bound::Included(ref y)) => item >= x && item <= y,
        }
    }
}

#[cfg(test)]
mod tests {
    use core::ops::Bound;

    use crate::GenericRange;

    #[test]
    fn in_ex() {
        assert!(!GenericRange::from(1..1).contains(&1));
        assert!(GenericRange::from(1..2).contains(&1));
        assert!(!GenericRange::from(1..2).contains(&2));
    }

    #[test]
    fn in_in() {
        assert!(GenericRange::from(1..=1).contains(&1));
        assert!(GenericRange::from(1..=2).contains(&1));
        assert!(GenericRange::from(1..=2).contains(&2));
    }

    #[test]
    fn in_unbound() {
        assert!(GenericRange::from(1..).contains(&1));
        assert!(GenericRange::from(1..).contains(&2));
    }

    #[test]
    fn unbound_ex() {
        assert!(GenericRange::from(..2).contains(&1));
        assert!(!GenericRange::from(..2).contains(&2));
    }

    #[test]
    fn unbound_in() {
        assert!(GenericRange::from(..=2).contains(&1));
        assert!(GenericRange::from(..=2).contains(&2));
    }

    #[test]
    fn unbound() {
        let generic: GenericRange<usize> = GenericRange::from(..);
        assert!(generic.contains(&1));
        assert!(generic.contains(&2));
    }

    #[test]
    fn ex_ex() {
        assert!(!GenericRange::from((Bound::Excluded(1), Bound::Excluded(1))).contains(&1));
        assert!(!GenericRange::from((Bound::Excluded(1), Bound::Excluded(2))).contains(&1));
        assert!(!GenericRange::from((Bound::Excluded(1), Bound::Excluded(2))).contains(&2));
    }

    #[test]
    fn ex_in() {
        assert!(!GenericRange::from((Bound::Excluded(1), Bound::Included(1))).contains(&1));
        assert!(!GenericRange::from((Bound::Excluded(1), Bound::Included(2))).contains(&1));
        assert!(GenericRange::from((Bound::Excluded(1), Bound::Included(2))).contains(&2));
    }

    #[test]
    fn ex_unbound() {
        assert!(!GenericRange::from((Bound::Excluded(1), Bound::Unbounded)).contains(&1));
        assert!(GenericRange::from((Bound::Excluded(1), Bound::Unbounded)).contains(&2));
    }
}

#[cfg(all(test, feature = "noisy_float"))]
mod tests_continuous {
    use core::ops::Bound;

    use noisy_float::types::N64;

    use crate::GenericRange;

    #[test]
    fn in_ex() {
        let one = N64::new(1.);
        let one_half = N64::new(1.5);
        let two = N64::new(2.);

        assert!(!GenericRange::from(one..one).contains(&one));
        assert!(GenericRange::from(one..two).contains(&one));
        assert!(GenericRange::from(one..two).contains(&one_half));
        assert!(!GenericRange::from(one..two).contains(&two));
    }

    #[test]
    fn in_in() {
        let one = N64::new(1.);
        let one_half = N64::new(1.5);
        let two = N64::new(2.);

        assert!(GenericRange::from(one..=one).contains(&one));
        assert!(GenericRange::from(one..=two).contains(&one));
        assert!(GenericRange::from(one..=two).contains(&one_half));
        assert!(GenericRange::from(one..=two).contains(&two));
    }

    #[test]
    fn in_unbound() {
        let one = N64::new(1.);
        let one_half = N64::new(1.5);
        let two = N64::new(2.);

        assert!(GenericRange::from(one..).contains(&one));
        assert!(GenericRange::from(one..).contains(&one_half));
        assert!(GenericRange::from(one..).contains(&two));
    }

    #[test]
    fn unbound_ex() {
        let one = N64::new(1.);
        let one_half = N64::new(1.5);
        let two = N64::new(2.);

        assert!(GenericRange::from(..two).contains(&one));
        assert!(GenericRange::from(..two).contains(&one_half));
        assert!(!GenericRange::from(..two).contains(&two));
    }

    #[test]
    fn unbound_in() {
        let one = N64::new(1.);
        let one_half = N64::new(1.5);
        let two = N64::new(2.);

        assert!(GenericRange::from(..=two).contains(&one));
        assert!(GenericRange::from(..=two).contains(&one_half));
        assert!(GenericRange::from(..=two).contains(&two));
    }

    #[test]
    fn unbound() {
        let one = N64::new(1.);
        let two = N64::new(2.);

        let generic: GenericRange<N64> = GenericRange::from(..);
        assert!(generic.contains(&one));
        assert!(generic.contains(&two));
    }

    #[test]
    fn ex_ex() {
        let one = N64::new(1.);
        let one_half = N64::new(1.5);
        let two = N64::new(2.);

        assert!(!GenericRange::from((Bound::Excluded(one), Bound::Excluded(one))).contains(&one));
        assert!(!GenericRange::from((Bound::Excluded(one), Bound::Excluded(two))).contains(&one));
        assert!(GenericRange::from((Bound::Excluded(one), Bound::Excluded(two))).contains(&one_half));
        assert!(!GenericRange::from((Bound::Excluded(one), Bound::Excluded(two))).contains(&two));
    }

    #[test]
    fn ex_in() {
        let one = N64::new(1.);
        let one_half = N64::new(1.5);
        let two = N64::new(2.);

        assert!(!GenericRange::from((Bound::Excluded(one), Bound::Included(one))).contains(&one));
        assert!(!GenericRange::from((Bound::Excluded(one), Bound::Included(two))).contains(&one));
        assert!(GenericRange::from((Bound::Excluded(one), Bound::Included(two))).contains(&one_half));
        assert!(GenericRange::from((Bound::Excluded(one), Bound::Included(two))).contains(&two));
    }

    #[test]
    fn ex_unbound() {
        let one = N64::new(1.);
        let one_half = N64::new(1.5);
        let two = N64::new(2.);

        assert!(!GenericRange::from((Bound::Excluded(one), Bound::Unbounded)).contains(&one));
        assert!(GenericRange::from((Bound::Excluded(one), Bound::Unbounded)).contains(&one_half));
        assert!(GenericRange::from((Bound::Excluded(one), Bound::Unbounded)).contains(&two));
    }
}
