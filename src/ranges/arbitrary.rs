use arbitrary::{Arbitrary, Result, Unstructured};

use crate::{Domain, GenericRange, Ranges};

impl<'a, T: Arbitrary<'a> + Domain> Arbitrary<'a> for Ranges<T> {
    fn arbitrary(u: &mut Unstructured<'a>) -> Result<Self> {
        let len = u.arbitrary_len::<GenericRange<T>>()?;
        let mut vec = Self::with_capacity(len);

        for _ in 0..len {
            let _ = vec.insert(u.arbitrary::<GenericRange<T>>()?);
        }

        Ok(vec)
    }
}
