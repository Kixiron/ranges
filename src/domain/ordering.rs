use core::cmp::Ordering::{self, Equal, Greater, Less};
use core::ops::Bound;

use crate::Domain;

impl Domain for Ordering {
    const DISCRETE: bool = true;

    fn predecessor(&self) -> Option<Self> {
        match *self {
            Less => None,
            Equal => Some(Less),
            Greater => Some(Equal),
        }
    }

    fn successor(&self) -> Option<Self> {
        match *self {
            Less => Some(Equal),
            Equal => Some(Greater),
            Greater => None,
        }
    }

    fn minimum() -> Bound<Self> {
        Bound::Included(Less)
    }

    fn maximum() -> Bound<Self> {
        Bound::Included(Greater)
    }

    fn is_next_to(&self, other: &Self) -> bool {
        match (*self, *other) {
            // when they're equal
            (Less, Less)
            | (Equal, Equal)
            | (Greater, Greater)
            // or have `Equal` in between
            | (Less, Greater)
            | (Greater, Less)
             => false,
            (Less, Equal)
            | (Equal, Less)
            | (Equal, Greater)
            | (Greater, Equal) => true,
        }
    }

    fn shares_neighbour_with(&self, other: &Self) -> bool {
        match (*self, *other) {
            // when they're next to `Equal`
            | (Equal, _)
            | (_, Equal)
            // when they're equal
            | (Less, Less)
            | (Greater, Greater) => false,
            (Less, Greater) | (Greater, Less) => true,
        }
    }
}

#[cfg(test)]
mod tests {
    use core::cmp::Ordering::{Equal, Greater, Less};

    use crate::Domain;

    #[test]
    fn is_next_to() {
        assert!(!Less.is_next_to(&Less));
        assert!(Less.is_next_to(&Equal));
        assert!(!Less.is_next_to(&Greater));
        assert!(Equal.is_next_to(&Less));
        assert!(!Equal.is_next_to(&Equal));
        assert!(Equal.is_next_to(&Greater));
        assert!(!Greater.is_next_to(&Less));
        assert!(Greater.is_next_to(&Equal));
        assert!(!Greater.is_next_to(&Greater));
    }
    #[test]
    fn shares_neighbour_with() {
        assert!(!Less.shares_neighbour_with(&Less));
        assert!(!Less.shares_neighbour_with(&Equal));
        assert!(Less.shares_neighbour_with(&Greater));
        assert!(!Equal.shares_neighbour_with(&Less));
        assert!(!Equal.shares_neighbour_with(&Equal));
        assert!(!Equal.shares_neighbour_with(&Greater));
        assert!(Greater.shares_neighbour_with(&Less));
        assert!(!Greater.shares_neighbour_with(&Equal));
        assert!(!Greater.shares_neighbour_with(&Greater));
    }
    #[test]
    fn predecessor() {
        assert_eq!(Less.predecessor(), None);
        assert_eq!(Equal.predecessor(), Some(Less));
        assert_eq!(Greater.predecessor(), Some(Equal));
    }
    #[test]
    fn successor() {
        assert_eq!(Less.successor(), Some(Equal));
        assert_eq!(Equal.successor(), Some(Greater));
        assert_eq!(Greater.successor(), None);
    }
}
