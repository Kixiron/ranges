use core::cmp::Ordering;
use core::ops::Bound;

use super::{Domain, Iterable};

impl Domain for u128 {
    const DISCRETE: bool = true;

    /// Always returns `Some(self - 1)` unless `self` is zero.
    #[must_use]
    #[allow(clippy::integer_arithmetic)]
    fn predecessor(&self) -> Option<Self> {
        match *self {
            Self::MIN => None,
            _ => Some(self - 1),
        }
    }

    /// Always returns `Some(self + 1)` unless `self` is `Self::MAX`.
    #[must_use]
    #[allow(clippy::integer_arithmetic)]
    fn successor(&self) -> Option<Self> {
        match *self {
            Self::MAX => None,
            _ => Some(self + 1),
        }
    }

    /// Returns `Included(Self::MIN)`.
    #[must_use]
    fn minimum() -> Bound<Self> {
        Bound::Included(Self::MIN)
    }

    /// Returns `Included(Self::MAX)`.
    #[must_use]
    fn maximum() -> Bound<Self> {
        Bound::Included(Self::MAX)
    }

    #[must_use]
    #[allow(clippy::shadow_reuse, clippy::integer_arithmetic)]
    fn shares_neighbour_with(&self, other: &Self) -> bool {
        let (big, small) = match self.cmp(other) {
            Ordering::Less => (other, self),
            Ordering::Equal => return false,
            Ordering::Greater => (self, other),
        };

        big - small == 2
    }
}

impl Iterable for u128 {
    type Output = Self;

    fn next(&self) -> Option<Self::Output> {
        if *self == Self::MAX {
            None
        } else {
            #[allow(clippy::integer_arithmetic)]
            Some(*self + 1)
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::Domain;

    #[test]
    fn is_next_to() {
        assert!(u128::MIN.is_next_to(&(u128::MIN + 1)));
        assert!((u128::MIN + 1).is_next_to(&u128::MIN));
        assert!(!u128::MIN.is_next_to(&(u128::MIN + 2)));
        assert!(!u128::MIN.is_next_to(&u128::MAX));
        assert!(!u128::MAX.is_next_to(&u128::MIN));
    }

    #[test]
    fn shares_neighbour_with() {
        // self-distance
        assert_eq!(u128::MIN.shares_neighbour_with(&u128::MIN), false);

        // "normal" value
        assert_eq!(42_u128.shares_neighbour_with(&45), false);
        assert_eq!(45_u128.shares_neighbour_with(&42), false);

        assert_eq!(42_u128.shares_neighbour_with(&44), true);
        assert_eq!(44_u128.shares_neighbour_with(&42), true);

        // boundary check
        assert_eq!(u128::MIN.shares_neighbour_with(&u128::MAX), false);
        assert_eq!(u128::MAX.shares_neighbour_with(&u128::MIN), false);
    }
}
