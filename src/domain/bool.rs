use core::ops::Bound;

use super::{Domain, Iterable};

impl Domain for bool {
    const DISCRETE: Self = true;

    /// Returns:
    ///
    /// |  `self` |     Output   |
    /// |---------|--------------|
    /// |   true  |  Some(false) |
    /// |  false  |     None     |
    #[must_use]
    fn predecessor(&self) -> Option<Self> {
        match self {
            true => Some(false),
            false => None,
        }
    }

    /// Returns:
    ///
    /// |  `self` |    Output    |
    /// |---------|--------------|
    /// |   true  |     None     |
    /// |  false  |  Some(true)  |
    #[must_use]
    fn successor(&self) -> Option<Self> {
        match self {
            true => None,
            false => Some(true),
        }
    }

    /// Returns `Included(false)`.
    #[must_use]
    fn minimum() -> Bound<Self> {
        Bound::Included(false)
    }

    /// Returns `Included(true)`.
    #[must_use]
    fn maximum() -> Bound<Self> {
        Bound::Included(true)
    }

    #[must_use]
    fn is_next_to(&self, other: &Self) -> bool {
        self != other
    }

    #[must_use]
    fn shares_neighbour_with(&self, _other: &Self) -> bool {
        false
    }
}

impl Iterable for bool {
    type Output = Self;

    #[must_use]
    fn next(&self) -> Option<Self::Output> {
        self.successor()
    }
}

#[cfg(test)]
mod tests {
    use crate::Domain;

    #[test]
    fn is_next_to() {
        assert!(!true.is_next_to(&true));
        assert!(true.is_next_to(&false));
        assert!(false.is_next_to(&true));
        assert!(!false.is_next_to(&false));
    }
    #[test]
    fn shares_neighbour_with() {
        assert!(!true.shares_neighbour_with(&true));
        assert!(!true.shares_neighbour_with(&false));
        assert!(!false.shares_neighbour_with(&true));
        assert!(!false.shares_neighbour_with(&false));
    }
    #[test]
    fn predecessor() {
        assert_eq!(true.predecessor(), Some(false));
        assert_eq!(false.predecessor(), None);
    }
    #[test]
    fn successor() {
        assert_eq!(true.successor(), None);
        assert_eq!(false.successor(), Some(true));
    }
}
