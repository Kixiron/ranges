use criterion::{black_box, criterion_group, criterion_main, BatchSize, BenchmarkId, Criterion, Throughput};

use ranges::{GenericRange, Ranges};

fn union(c: &mut Criterion) {
    let mut group = c.benchmark_group("union collisions");
    let max_exponent = 14;

    for exponent in 0..=max_exponent {
        let mut ranges = Ranges::new();
        for i in 0..2_usize.pow(exponent) {
            ranges.insert(i * 10..(i * 10) + 5);
        }

        group.throughput(Throughput::Elements(ranges.len() as u64));
        group.bench_function(BenchmarkId::from_parameter(ranges.len()), |b| {
            b.iter_batched(
                || ranges.clone(),
                |r| {
                    let _r = r.union(black_box(GenericRange::from(usize::MIN..=usize::MAX)));
                },
                BatchSize::PerIteration,
            );
        });
    }

    group.finish();
}

criterion_group!(benches, union);
criterion_main!(benches);
