#!/bin/sh -xe

cargo clippy --release --all-features -- \
`# Forbid: Clippy Groups` \
--forbid clippy::complexity \
--forbid clippy::correctness \
--forbid clippy::perf \
--forbid clippy::style \
\
`# Deny: Clippy Groups` \
--deny clippy::nursery \
--deny clippy::pedantic \
--deny clippy::restriction \
\
`# Allow: Clippy Groups` \
--allow clippy::implicit-return \
--allow clippy::missing-inline-in-public-items \
--allow clippy::redundant-pub-crate \
--allow clippy::unreachable \
--allow clippy::use-self `# unreachable!() triggers this` \
\
`# Forbid: RustC Groups` \
--forbid future-incompatible \
--forbid nonstandard-style \
--forbid rust-2018-compatibility \
--forbid rust-2018-idioms \
--forbid rustdoc \
\
`# Forbid: RustC Lints` \
--forbid box-pointers \
--forbid deprecated-in-future \
--forbid macro-use-extern-crate \
--forbid meta-variable-misuse \
--forbid missing-copy-implementations \
--forbid missing-crate-level-docs \
--forbid missing-debug-implementations \
--forbid non-ascii-idents \
--forbid single-use-lifetimes \
--forbid trivial-casts \
--forbid trivial-numeric-casts \
--forbid unaligned-references \
--forbid unreachable-pub \
--forbid unsafe-code \
--forbid unstable-features \
--forbid unused-import-braces \
--forbid unused-lifetimes \
--forbid variant-size-differences \
--forbid asm-sub-register \
--forbid bindings-with-variant-name \
--forbid clashing-extern-declarations \
--forbid confusable-idents \
--forbid const-item-mutation \
--forbid deprecated \
--forbid drop-bounds \
--forbid exported-private-dependencies \
--forbid function-item-references \
--forbid improper-ctypes \
--forbid improper-ctypes-definitions \
--forbid incomplete-features \
--forbid inline-no-sanitize \
--forbid invalid-value \
--forbid irrefutable-let-patterns \
--forbid missing-docs \
--forbid mixed-script-confusables \
--forbid non-shorthand-field-patterns \
--forbid no-mangle-generic-items \
--forbid renamed-and-removed-lints \
--forbid stable-features \
--forbid temporary-cstring-as-ptr \
--forbid trivial-bounds \
--forbid type-alias-bounds \
--forbid uncommon-codepoints \
--forbid unconditional-recursion \
--forbid unknown-lints \
--forbid unnameable-test-items \
--forbid unused-crate-dependencies \
--forbid unused-comparisons \
--forbid unused-crate-dependencies \
--forbid unused-results \
--forbid warnings \
--forbid while-true \
--forbid arithmetic-overflow \
--forbid const-err \
--forbid incomplete-include \
--forbid mutable-transmutes \
--forbid no-mangle-const-items \
--forbid overflowing-literals \
--forbid unconditional-panic \
--forbid unknown-crate-types \
--forbid useless-deprecated \
\
`# Deny: RustC Lints` \
--deny unreachable-code \
--deny unused \
\
`# Allow: RustC Lints` \
--allow unused-qualifications \

